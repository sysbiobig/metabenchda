In this folder all users can see the original datasets used in our benchmarking study. The "generate_dataset.R" file contains the code used to filter the original otu matrix and to create the datasets used in benchmarking. 

* HMP: These data were obtained in the context of the Human Microbiome Project ([HMP](https://doi.org/10.1038/nature11234)) and, among the microbial communities that characterise the oral cavity, we extracted the one related to the teeth site.
* ID_12021: These data come from a study conducted by [He](https://doi.org/10.1038/s41598-019-47953-4) et al. on the effect of the type of feeding in infants. The count table and metadata are available in the QIITA database (https://qiita.ucsd.edu/study/description/12021#). We focussed on the breast-fed (BF) reference group by choosing the “6 month” time point.
* IBD: these data come from The Inflammatory Bowel Disease Multi'omics Database ([IBDMD](https://doi.org/10.1038/s41586-019-1237-9)) a study conducted as part of the Integrative Human Microbiome Project (HMP2 or iHMP) to characterise gut microbial ecosystem in the context of Inflammatory Bowel Disease (IBD). Data are publicly available from the project reference site (https://ibdmdb.org/). We extracted abundance profiles related to the control condition of healthy subjects (nonIBD). 
* soil: these data comes from a study conducted by [Yurgel et al.](https://doi.org/10.3389/fmicb.2018.01187) on the differences between bacterial and eukaryotic soil communities associated with natural and managed habitats of wild blueberry. We download already preprocessed data from [Nearing et al.](https://doi.org/10.1038/s41467-022-28034-z) and we focus on Soil condition. 
* The last dataset is obtained from the preset available in [metaSPARSim](https://doi.org/10.1186/s12859-019-2882-6) R package, i.e. AnimalGut. 

All subjects’ profile with sequencing depth between 5000 and 25000 are retained. In addition, taxa with zero values across all the samples considered in our analysis were filtered out.

All datasets are accessible via the data command: 

`data("NAME_DATASET")` 

where NAME_DATASET is the name of the condition extracted from the original dataset (BF, nonIBD, tooth, Soil and AnimalGut). 

each dataset is a list that contains:

* counts: the otu table with taxa on rows and samples on columns. 
* params: a list of parameters estimated from the otu table: intensity, variability and library size.

In addition, we provide two nonIBD variants with half and double library size vector (NAME_DATASET= "nonIBDdouble" or NAME_DATASET= "nonIBDhalf").






