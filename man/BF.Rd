% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/metaBenchDA-data.R
\docType{data}
\name{BF}
\alias{BF}
\title{DATASET BF}
\format{
A list with the following elements:
\describe{
\item{counts}{the otu table with taxa on rows and samples on columns.}
\item{params}{ a list of parameters estimated from the otu table: intensity, variability and library size.}
}
}
\usage{
data(BF)
}
\description{
These data were obtained in the context of the Human Microbiome Project (HMP) and, among the microbial communities that characterise the oral cavity, we extracted the one related to the teeth site.
}
\examples{
# load dataset
data(BF)
}
\keyword{datasets}
