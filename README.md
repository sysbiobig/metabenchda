# metaBenchDA: Investigating differential abundance methods in microbiome data: a benchmark study

This is the repository containing the code and the data used to obtain the results shown in *Cappellato M., Baruzzo G., Di Camillo B. "Investigating differential abundance methods in microbiome data: a benchmark study." (2021)*.

The repository contains the R package **metaBenchDA**; the R package contains the data and the code to perform the simulation framework, to run all the differential abundance analysis methods and to assess methods' performance. 

The repository contains also the Docker image **metabenchda:2.0.0**; the Docker image contains the R package **metaBenchDA** and the code to reproduce the results shown in the paper. 

## How to install

**metaBenchDA** is an R (https://www.r-project.org/) package available on GitLab at https://gitlab.com/sysbiobig/metabenchda 

To install **metaBenchDA** from GitLab, please use the following commands:

```r
Sys.setenv(R_REMOTES_NO_ERRORS_FROM_WARNINGS="true")
library(devtools)
install_gitlab("sysbiobig/metabenchda", build_opts = c("--no-resave-data", "--no-manual"), build_vignettes = FALSE)
```

## How to run 

Open a R session and load the library using the command:

```r
library(metaBenchDA)
```
then follow these steps:

1. Download **`analysis_without_DA_taxa.R`** from [here](https://gitlab.com/sysbiobig/metabenchda/-/raw/master/analysis_without_DA_taxa.R?inline=false)
2. Download **`analysis_with_DA_taxa.R`** from [here](https://gitlab.com/sysbiobig/metabenchda/-/raw/master/analysis_with_DA_taxa.R?inline=false)
3. Download **`figures.R`** from [here](https://gitlab.com/sysbiobig/metabenchda/-/raw/master/figures.R?inline=false)
4. Download **`results`** from [here](https://doi.org/10.5281/zenodo.5799193)
5. Follow the instruction in the scripts just downloaded (for more details see Script section below).

## Alternative: How to install & run using Docker container

A Docker (https://www.docker.com/) container is available at https://gitlab.com/sysbiobig/metabenchda/container_registry/

Open a terminal and start the Docker container using the command:

```r
docker run --name metabenchda -p 127.0.0.1:8787:8787 -d -e DISABLE_AUTH=true registry.gitlab.com/sysbiobig/metabenchda:2.0.0
```

then open browser on **localhost:8787** to work with Rstudio inside the container image.

The **metabenchda:2.0.0** Docker image has already installed the library **metaBenchDA** and it contains all the scripts to reproduce our work and the results folder (for more details see Script section below).

#### Using container with local folder
If you want to run container and bind your local folder to load your custom data or access result files in your local machine you can start container in this way:

```r
docker run --name metabenchda -p 127.0.0.1:8787:8787 -d -v ~/[YOUR_LOCAL_FOLDER_PATH]:/home/rstudio/[FOLDER_NAME_INSIDE_CONTAINER] -e DISABLE_AUTH=true registry.gitlab.com/sysbiobig/metabenchda:2.0.0
```

Again, open browser on **localhost:8787** to work with Rstudio inside the container image.


## Script 
The script file **`analysis_without_DA_taxa.R`**, **`analysis_with_DA_taxa.R`**, **`figures.R`**  contain the code to obtain the results and to reproduce the plots shown in manuscript and supplementary materials. 

In particular:

* **`analysis_without_DA_taxa.R`** provide the code to reproduce results about the simulation without DA features.

* **`analysis_with_DA_taxa.R`** provide the code to reproduce results about all the scenarios tested with DA features. 

* **`figures.R`** provide the code to reproduce figures.

The code in **`analysis_without_DA_taxa.R`** and **`analysis_with_DA_taxa.R`** is structured in several sections:

* **SECTION 1: GENERATE DATASET** This section loads the data and simulate all the otu table based on the scenario chosen by the user. 

* **SECTION 2: RUN DA METHODS** This section runs all differential abundance methods implemented in the package on data simulated in the previous section. Again, the user can specify the scenario and the methods to run. 

* **SECTION 3: RUN METRICS** This section calculates all the metrics implemented in the package.

Since the whole computation, i.e. simulation, method run and assessment, for all the scenarios tested in the article can take a long time, we provide the **`results`** folder which contains all the files needed to run the code in every point of the framework. The **`results`** folder contains:

* **lib_unbalanced** results in the scenario with DA features and setting both the intensity threshold and unbalanced sequencing depth between conditions.

* **metagenomeSeq_res** results running two models implemented in metagenomeSeq, namely the zero-inflated Gaussian (ZIG) model and the zero-inflated Log-Gaussian (ZILG) mixture model, in the scenario with DA features and setting the intensity threshold.

* **new_datasets** results in the scenario with DA features and setting the intensity threshold for AnimalGut and Soil datasets. 

* **NOth** results in the scenario with DA features and without setting the intensity threshold.

* **NULL** results in the scenario without DA features.

* **WITHth** results in the scenario with DA features and setting the intensity threshold.

* **WITHth_GMPR** results in the scenario with DA features and setting the intensity threshold. We test the methods with [GMPR](https://doi.org/10.7717/peerj.4600) normalisation.

* **WITHth_HALFvar** results in the scenario with DA features and decreasing the variability parameter.

Inside the folders the results considering structural zeros as TN are available in the **`reassign`** folder, while considering the choice of methods in **`methodoutput`**. For more detail about the **results** folder click [here](https://doi.org/10.5281/zenodo.5799193)

If you run the scripts inside the container, all the files created by the scripts are saved in **`results`** folder inside the container. You can download from **more -> export** button in RStudio. 

## Dataset
Please, see the **`original data`** folder [here](https://gitlab.com/sysbiobig/metabenchda/-/tree/master/original%20data) for more details about the datasets used.
