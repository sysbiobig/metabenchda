# IMAGE: 1.0.0

# R version 4.0.0 (2020-04-24) -- "Arbor Day"
FROM bioconductor/bioconductor_docker:RELEASE_3_11

# deps from cran
RUN install2.r --error doParallel foreach pracma nloptr Rdpack nlme ggplot2 ggpubr dplyr compositions tidyr magrittr pscl

# deps from bioconductor
RUN Rscript -e 'requireNamespace("BiocManager"); BiocManager::install(c("phyloseq", "biomformat", "scran", "ALDEx2", "metagenomeSeq", "Biobase", "microbiome", "edgeR","DESeq2"));'

# deps from github
RUN Rscript -e 'library(devtools); install_github("lichen-lab/GMPR");'
RUN Rscript -e 'library(devtools); install_github("liudoubletian/eBay");'
RUN Rscript -e 'library(devtools); install_github("yuanjing-ma/RioNorm2");'
RUN Rscript -e 'library(devtools); install_github("bryandmartin/corncob");'

# deps from gitlab
RUN Rscript -e 'library(devtools); install_gitlab("sysbiobig/metaSPARSim");'

# deps from other sources
COPY ./docker/deps deps
RUN Rscript -e 'install.packages("deps/ANCOMBC_1.0.2.tar.gz")'
RUN rm -rf /tmp/downloaded_packages/ /tmp/*.rds

RUN echo build v.2.0.0-0

# copy file inside container
COPY figures.R /home/rstudio/figures.R
COPY analysis_with_DA_taxa.R /home/rstudio/analysis_with_DA_taxa.R
COPY analysis_without_DA_taxa.R /home/rstudio/analysis_without_DA_taxa.R
COPY results /home/rstudio/results
COPY README.md /home/rstudio/README.md

RUN chmod -R 777 /home/rstudio/figures.R
RUN chmod -R 777 /home/rstudio/analysis_with_DA_taxa.R
RUN chmod -R 777 /home/rstudio/analysis_without_DA_taxa.R
RUN chmod -R 777 /home/rstudio/results
RUN chmod -R 777 /home/rstudio/README.md

RUN Rscript -e 'requireNamespace("BiocManager"); BiocManager::install("Maaslin2");'

# install metabenchda:
RUN Rscript -e 'Sys.setenv(R_REMOTES_NO_ERRORS_FROM_WARNINGS="true"); library(devtools); install_gitlab("sysbiobig/metabenchda", build_opts = c("--no-resave-data", "--no-manual"), build_vignettes = FALSE);'





