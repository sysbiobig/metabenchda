### Build image

```
cd metabenchda
docker build -t registry.gitlab.com/sysbiobig/metabenchda:x.x.x .
```

### Rebuild image and update to registry

```
docker build -t registry.gitlab.com/sysbiobig/metabenchda:x.x.x .
docker login registry.gitlab.com
docker push registry.gitlab.com/sysbiobig/metabenchda:x.x.x
```

### Run official image
```
docker run --name metabenchda -p 127.0.0.1:8787:8787 -d -e DISABLE_AUTH=true registry.gitlab.com/sysbiobig/metabenchda:x.x.x
```

If you want bind R project inside the image (for developers):

```
docker run --name metabenchda -p 127.0.0.1:8787:8787 -d -v ~/[YOUR_FOLDER_REPOS]:/home/rstudio/repos -e DISABLE_AUTH=true registry.gitlab.com/sysbiobig/metabenchda:x.x.x
```

